﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Collections;
using System.IO;
using System.Data;

namespace GeneraXMLGBIF
{
    class DBAccess
    {
        OleDbDataReader reader;
        OleDbCommand command;
        OleDbConnection connection;
        string connectionString;
        
        public DBAccess(string source, string password)
        {
            this.connection = null;
            this.connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + source + ";Jet OLEDB:Database Password=" + password + ";";

            this.connection = new OleDbConnection(this.connectionString);
        }

        /// <summary>
        /// Closes the active connection to the database.
        /// </summary>
        public void Close()
        {
            this.connection.Close();
        }

        /// <summary>
        /// Open the active connection to the database.
        /// </summary>
        public void Open()
        {
            this.connection.Open();
        }
        /// <summary>
        /// Gets the data from the associated table.
        /// </summary>
        /// &ltparam name="table">The table to collect data from.</param>
        /// &ltparam name="ordering">The column by which to order the rows.</param>
        /// &ltreturns>An ArrayList of ArrayLists pertaining to the rows in the table.</returns>
        public ArrayList Select(string table, string ordering)
        {
            return this.ExplicitQuery("SELECT * FROM " + table + " ORDER BY " + ordering);
        }


        public OleDbDataReader ExecuteNonQuery(string query)
        {
            OleDbDataReader objDataReader = null;
            try
            {
                this.command = new OleDbCommand(query, connection);
                objDataReader = command.ExecuteReader();
                
                return objDataReader;
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show(e.ToString());
                return null;

            }


        }

        /// <summary>
        /// Changes data within a certain table.
        /// </summary>
        /// &ltparam name="table">The table whose data is to be changed.</param>
        /// &ltparam name="schema">The names of the columns in the table to be changed.</param>
        /// &ltparam name="keys">The indexes of the keys within the schema.</param>
        /// &ltparam name="fields">The new values of the row(s) to be updated.</param>
        /// &ltreturns>True if at least one row was changed, otherwise false.</returns>


        /// <summary>
        /// Executes a specific query or stored procedure.
        /// </summary>
        /// &ltparam name="query">The name of a stored procedure or a query written in SQL.</param>
        /// &ltreturns></returns>
        public bool ExplicitNonQuery(string query)
        {
            try
            {
                this.command.CommandText = query;
                return this.command.ExecuteNonQuery() > 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Executes a specific query or stored procedure to obtain row data.
        /// </summary>
        /// &ltparam name="query">The name of a stored procedure or a query written in SQL.</param>
        /// &ltreturns>An ArrayList corresponding to the selected rows of the query or stored procedure.</returns>
        public ArrayList ExplicitQuery(string query)
        {
            try
            {
                ArrayList array = new ArrayList();
                this.command.CommandText = query;
                this.reader = this.command.ExecuteReader();
                while (this.reader.Read())
                {
                    ArrayList a = new ArrayList();
                    for (int i = 0; i < this.reader.FieldCount; i++)
                        a.Add(this.reader[i]);
                    array.Add(a);
                }
                this.reader.Close();
                return array;
            }
            catch 
            {
                return new ArrayList();
            }
        }       
    }
}
