﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace GeneraXMLGBIF
{
    class XML_GBIF
    {
        XmlTextWriter writer;
        public string xmlPath;
        public string constr;
        public string name;
        public string contacAdmin;
        public string contacTitle;
        public string contacEmail;
        public string abs;
        public string citation;
        public string identifier;
        public string[] camposDwC = { "darwin:DateLastModified,1",
                                    "darwin:InstitutionCode,2", 
                                    "darwin:CollectionCode,3",
                                    "darwin:CatalogNumber,4",
                                    "darwin:ScientificName,5",
                                    "darwin:BasisOfRecord,6",
                                    "darwin:Kingdom,7",
                                    "darwin:Phylum,8",
                                    "darwin:Class,9",
                                    "darwin:Order,10",
                                    "darwin:Family,11",
                                    "darwin:Genus,12",
                                    "darwin:Species,13",
                                    "darwin:Subspecies,14",
                                    "darwin:ScientificNameAuthor,15",
                                    "darwin:IdentifiedBy,16",
                                    "darwin:YearIdentified,17",
                                    "darwin:MonthIdentified,18",
                                    "darwin:DayIdentified,19",
                                    "darwin:TypeStatus,20",
                                    "darwin:CollectorNumber,21",
                                    "darwin:Collector,23",
                                    "darwin:YearCollected,24",
                                    "darwin:MonthCollected,25",
                                    "darwin:DayCollected,26",
                                    "darwin:JulianDay,numeric,27",
                                    "darwin:TimeOfDay,numeric,28",
                                    "darwin:Country,30",
                                    "darwin:StateProvince,31",
                                    "darwin:County,32",
                                    "darwin:Locality,33",
                                    "darwin:Longitude,34",
                                    "darwin:Latitude,35",
                                    "darwin:MinimumElevation,numeric,38",
                                    "darwin:MaximumElevation,numeric,39",
                                    "darwin:MinimumDepth,numeric,40",
                                    "darwin:MaximumDepth,numeric,41",
                                    "darwin:Sex,42",
                                    "darwin:PreparationType,43",
                                    "darwin:IndividualCount,44",
                                    "darwin:Notes,48"};
        public string[] camposMDwC = { "modified",
                                    "institutionCode", 
                                    "collectionCode",
                                    "catalogNumber",
                                    "scientificName",
                                    "basisOfRecord",
                                    "kingdom",
                                    "phylum",
                                    "class",
                                    "order",
                                    "family",
                                    "genus",
                                    "specificEpithet",
                                    "infraspecificEpithet",
                                    "scientificNameAuthorship",
                                    "identifiedBy",
                                    "yearIdentified",
                                    "monthIdentified",
                                    "dayIdentified",
                                    "typeStatus",
                                    "fieldNumber",
                                    "recordedBy",
                                    "year",
                                    "month",
                                    "day",
                                    "startDayOfYear",
                                    "eventTime",
                                    "country",
                                    "stateProvince",
                                    "county",
                                    "locality",
                                    "decimalLongitude",
                                    "decimalLatitude",
                                    "minimumElevationInMeters",
                                    "maximumElevationInMeters",
                                    "minimumDepthInMeters",
                                    "maximumDepthInMeters",
                                    "sex",
                                    "preparations",
                                    "individualCount,",
                                    "notes" };

        public XML_GBIF(OleDbDataReader Metadatos, string path)
        {
            this.xmlPath = path + "\\" + Metadatos["code"].ToString().Trim() + ".xml";
            this.constr = Metadatos["ProviderString"] is DBNull ? "" : (string)Metadatos["ProviderString"];
            this.name = Metadatos["Name"] is DBNull ? "" : (string)Metadatos["Name"];
            this.contacAdmin = Metadatos["NameAdministrative"] is DBNull ? "" : (string)Metadatos["NameAdministrative"];
            this.contacTitle = Metadatos["TitleAdministrative"] is DBNull ? "" : (string)Metadatos["TitleAdministrative"];
            this.contacEmail = Metadatos["EmailAddress"] is DBNull ? "" : (string)Metadatos["EmailAddress"];
            this.abs = Metadatos["Abstract"] is DBNull ? "" : (string)Metadatos["Abstract"];
            this.citation = Metadatos["Citation"] is DBNull ? "" : (string)Metadatos["Citation"];
            this.identifier = Metadatos["RecordIdentifier"] is DBNull ? "" : (string)Metadatos["RecordIdentifier"];

            var utf8WithoutBom = new System.Text.UTF8Encoding(false);
            writer = new XmlTextWriter(xmlPath, utf8WithoutBom);
            writer.Formatting = Formatting.Indented;
        }

        //Destructor
        ~XML_GBIF()
        {
        }

        public void GeneraXML() 
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("configuration");
                writer.WriteStartElement("datasource");
                    writer.WriteAttributeString("type", "SQL");
                    writer.WriteAttributeString("constr", this.constr.Trim());
                    writer.WriteAttributeString("uid", "");
                    writer.WriteAttributeString("pwd", "");
                    writer.WriteAttributeString("database", "");
                    writer.WriteAttributeString("dbtype", "ado_access");
                    writer.WriteAttributeString("encoding", "ISO-8859-1");
                writer.WriteEndElement();
                writer.WriteStartElement("table");
                    writer.WriteAttributeString("name", "DarwinCore");
                    writer.WriteAttributeString("key", "datasetID");
                writer.WriteEndElement();
                writer.WriteStartElement("filter");
                writer.WriteEndElement();
                writer.WriteStartElement("concepts");
                    writer.WriteAttributeString("xmlns:darwin", "http://digir.net/schema/conceptual/darwin/2003/1.0");
                    for (int i = 0; i < camposDwC.Length; i++)
                    {
                        int x = new System.Text.RegularExpressions.Regex(",").Matches(camposDwC[i].ToString()).Count;
                        if (x > 1)
                        {
                            writer.WriteStartElement("concept");
                            writer.WriteAttributeString("searchable", "1");
                            writer.WriteAttributeString("returnable", "1");
                            writer.WriteAttributeString("name", camposDwC[i].ToString().Substring(0, camposDwC[i].ToString().IndexOf(",")));
                            writer.WriteAttributeString("type", camposDwC[i].ToString().Substring(camposDwC[i].ToString().IndexOf(",") + 1, (camposDwC[i].ToString().LastIndexOf(",")) - (camposDwC[i].ToString().IndexOf(",") + 1)));
                            writer.WriteAttributeString("table", "DarwinCore");
                            writer.WriteAttributeString("field", camposMDwC[i].ToString());
                            writer.WriteAttributeString("zid", camposDwC[i].ToString().Substring(camposDwC[i].ToString().LastIndexOf(",") + 1));
                            writer.WriteEndElement();
                        }
                        else 
                        {
                            writer.WriteStartElement("concept");
                            writer.WriteAttributeString("searchable", "1");
                            writer.WriteAttributeString("returnable", "1");
                            writer.WriteAttributeString("name", camposDwC[i].ToString().Substring(0, camposDwC[i].ToString().IndexOf(",")));
                            writer.WriteAttributeString("type", "text");
                            writer.WriteAttributeString("table", "DarwinCore");
                            writer.WriteAttributeString("field", camposMDwC[i].ToString());
                            writer.WriteAttributeString("zid", camposDwC[i].ToString().Substring(camposDwC[i].ToString().IndexOf(",") + 1));
                            writer.WriteEndElement();
                        }
                    }
                writer.WriteEndElement();
                writer.WriteStartElement("metadata");
                    writer.WriteElementString("code", "! set by application (value in resources.xml) !");
                    writer.WriteElementString("numberOfRecords", "! set by application !");
                    writer.WriteElementString("dateLastUpdated", "! set by application !");
                    writer.WriteElementString("name", this.name.Trim());
                    writer.WriteStartElement("relatedInformation");
                    writer.WriteEndElement();
                    writer.WriteStartElement("contact");
                        writer.WriteAttributeString("type", "technical");
                        writer.WriteElementString("name", "Sonia Alejandra Careaga Olvera");
                        writer.WriteElementString("title", "Bióloga");
                        writer.WriteElementString("emailAddress", "scareaga@conabio.gob.mx");
                        writer.WriteElementString("phone", "(52) 55-50044966");
                    writer.WriteEndElement();
                    writer.WriteStartElement("contact");
                        writer.WriteAttributeString("type", "administrative");
                        writer.WriteElementString("name", this.contacAdmin.Trim());
                        writer.WriteElementString("title", this.contacTitle.Trim());
                        writer.WriteElementString("emailAddress", this.contacEmail.Trim());
                        writer.WriteStartElement("phone");
                        writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteElementString("abstract", "El contenido de las bases de datos que publica la Comisión Nacional para el Conocimiento y uso de la Biodiversidad (CONABIO) a "+
                    "GBIF por medio del nodo mexicano refleja el resultado de un proceso de control de calidad que incluye estandarización y limpieza de los datos "+
                    "(http://www.snib.mx/d/CONABIO-SNIB-Version-201511.pdf). Las bases de datos originales pueden ser consultadas en el siguiente sitio: "+
                    "http://www.snib.mx/p/pf.html." + this.abs.Trim());
                    writer.WriteStartElement("keywords");
                    writer.WriteEndElement();
                    writer.WriteElementString("citation", this.citation.Trim());
                    writer.WriteStartElement("useRestrictions");
                    writer.WriteEndElement();
                    writer.WriteStartElement("conceptualSchema");
                        writer.WriteAttributeString("schemaLocation", "http://digir.net/schema/conceptual/darwin/2003/1.0/darwin2.xsd");
                        writer.WriteString("http://digir.net/schema/conceptual/darwin/2003/1.0");
                    writer.WriteEndElement();
                    writer.WriteElementString("recordIdentifier", this.identifier.Trim());
                    writer.WriteStartElement("recordBasis");
                    writer.WriteEndElement();
                    writer.WriteStartElement("minQueryTermLength");
                    writer.WriteEndElement();
                    writer.WriteStartElement("maxSearchResponseRecords");
                    writer.WriteEndElement();
                    writer.WriteStartElement("maxInventoryResponseRecords");
                    writer.WriteEndElement();
                writer.WriteEndElement();                   
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Flush();
            writer.Close();
        }
    }
}
