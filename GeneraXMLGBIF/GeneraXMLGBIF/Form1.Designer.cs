﻿namespace GeneraXMLGBIF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_conecta = new System.Windows.Forms.Button();
            this.contrasena = new System.Windows.Forms.TextBox();
            this.btn_rutabd = new System.Windows.Forms.Button();
            this.rutaBD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btn_genera = new System.Windows.Forms.Button();
            this.btn_destino = new System.Windows.Forms.Button();
            this.destino = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rutaXMLs = new System.Windows.Forms.TextBox();
            this.btn_rutaxml = new System.Windows.Forms.Button();
            this.btn_crearef = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.folderBrowserDialog2 = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_conecta);
            this.groupBox1.Controls.Add(this.contrasena);
            this.groupBox1.Controls.Add(this.btn_rutabd);
            this.groupBox1.Controls.Add(this.rutaBD);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Conexión a la base (Metadatos)";
            // 
            // btn_conecta
            // 
            this.btn_conecta.Location = new System.Drawing.Point(489, 42);
            this.btn_conecta.Name = "btn_conecta";
            this.btn_conecta.Size = new System.Drawing.Size(75, 23);
            this.btn_conecta.TabIndex = 5;
            this.btn_conecta.Text = "Conectar";
            this.btn_conecta.UseVisualStyleBackColor = true;
            this.btn_conecta.Click += new System.EventHandler(this.btn_conecta_Click);
            // 
            // contrasena
            // 
            this.contrasena.Location = new System.Drawing.Point(346, 45);
            this.contrasena.Name = "contrasena";
            this.contrasena.Size = new System.Drawing.Size(137, 20);
            this.contrasena.TabIndex = 4;
            // 
            // btn_rutabd
            // 
            this.btn_rutabd.Location = new System.Drawing.Point(301, 40);
            this.btn_rutabd.Name = "btn_rutabd";
            this.btn_rutabd.Size = new System.Drawing.Size(27, 23);
            this.btn_rutabd.TabIndex = 3;
            this.btn_rutabd.Text = "...";
            this.btn_rutabd.UseVisualStyleBackColor = true;
            this.btn_rutabd.Click += new System.EventHandler(this.btn_rutabd_Click);
            // 
            // rutaBD
            // 
            this.rutaBD.Location = new System.Drawing.Point(6, 42);
            this.rutaBD.Name = "rutaBD";
            this.rutaBD.Size = new System.Drawing.Size(289, 20);
            this.rutaBD.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(343, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Contraseña:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selecciona una Base:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.progressBar1);
            this.groupBox2.Controls.Add(this.btn_genera);
            this.groupBox2.Controls.Add(this.btn_destino);
            this.groupBox2.Controls.Add(this.destino);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(590, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 72);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(578, 22);
            this.progressBar1.TabIndex = 4;
            // 
            // btn_genera
            // 
            this.btn_genera.Location = new System.Drawing.Point(489, 19);
            this.btn_genera.Name = "btn_genera";
            this.btn_genera.Size = new System.Drawing.Size(95, 47);
            this.btn_genera.TabIndex = 3;
            this.btn_genera.Text = "Genera XML";
            this.btn_genera.UseVisualStyleBackColor = true;
            this.btn_genera.Click += new System.EventHandler(this.btn_genera_Click);
            // 
            // btn_destino
            // 
            this.btn_destino.Location = new System.Drawing.Point(450, 32);
            this.btn_destino.Name = "btn_destino";
            this.btn_destino.Size = new System.Drawing.Size(33, 23);
            this.btn_destino.TabIndex = 2;
            this.btn_destino.Text = "...";
            this.btn_destino.UseVisualStyleBackColor = true;
            this.btn_destino.Click += new System.EventHandler(this.btn_destino_Click);
            // 
            // destino
            // 
            this.destino.Location = new System.Drawing.Point(96, 34);
            this.destino.Name = "destino";
            this.destino.Size = new System.Drawing.Size(348, 20);
            this.destino.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Carpeta destino:";
            // 
            // rutaXMLs
            // 
            this.rutaXMLs.Location = new System.Drawing.Point(12, 142);
            this.rutaXMLs.Name = "rutaXMLs";
            this.rutaXMLs.Size = new System.Drawing.Size(295, 20);
            this.rutaXMLs.TabIndex = 2;
            // 
            // btn_rutaxml
            // 
            this.btn_rutaxml.Location = new System.Drawing.Point(313, 140);
            this.btn_rutaxml.Name = "btn_rutaxml";
            this.btn_rutaxml.Size = new System.Drawing.Size(27, 23);
            this.btn_rutaxml.TabIndex = 3;
            this.btn_rutaxml.Text = "...";
            this.btn_rutaxml.UseVisualStyleBackColor = true;
            this.btn_rutaxml.Click += new System.EventHandler(this.btn_rutaxml_Click);
            // 
            // btn_crearef
            // 
            this.btn_crearef.Location = new System.Drawing.Point(358, 140);
            this.btn_crearef.Name = "btn_crearef";
            this.btn_crearef.Size = new System.Drawing.Size(238, 23);
            this.btn_crearef.TabIndex = 4;
            this.btn_crearef.Text = "Crear resources.xml";
            this.btn_crearef.UseVisualStyleBackColor = true;
            this.btn_crearef.Click += new System.EventHandler(this.btn_crearef_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 186);
            this.Controls.Add(this.btn_crearef);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_rutaxml);
            this.Controls.Add(this.rutaXMLs);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GeneraXML";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox rutaBD;
        private System.Windows.Forms.TextBox contrasena;
        private System.Windows.Forms.Button btn_rutabd;
        private System.Windows.Forms.Button btn_conecta;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_genera;
        private System.Windows.Forms.Button btn_destino;
        private System.Windows.Forms.TextBox destino;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox rutaXMLs;
        private System.Windows.Forms.Button btn_rutaxml;
        private System.Windows.Forms.Button btn_crearef;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog2;
    }
}

