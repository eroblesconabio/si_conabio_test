﻿using System;
using System.Data.OleDb;
using System.Windows.Forms;
using System.IO;

namespace GeneraXMLGBIF
{
    public partial class Form1 : Form
    {
        private DBAccess DB;
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_genera_Click(object sender, EventArgs e)
        {
            if (destino.Text != "")
            {

                OleDbDataReader rd;
                OleDbDataReader rdCount;
                
                //hacemos una consulta a la base de datos
                DB.Open();              
                rdCount = DB.ExecuteNonQuery("Select count(*) as cnt From TablaFinal");
                rd = DB.ExecuteNonQuery("Select * From TablaFinal");


                btn_genera.Enabled = false;
                progressBar1.Visible = true;

                //Recorremos todos los datos
                if (rd.HasRows)
                {
                    btn_genera.Visible = false;
                    //Ponemos el valor para el progress bar
                    rdCount.Read();
                    progressBar1.Maximum = Int32.Parse(rdCount["cnt"].ToString());

                    try
                    {
                        while (rd.Read())
                        {
                            if (rd != null)
                            {
                                XML_GBIF eol = new XML_GBIF(rd, destino.Text);
                                eol.GeneraXML();
                            }

                            if (progressBar1.Value >= progressBar1.Maximum)
                                progressBar1.Value = 0;
                            else
                                progressBar1.Increment(1);

                        }
                        DB.Close();
                        MessageBox.Show("Se crearon los archivos XML con exito", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);


                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    finally
                    {

                        progressBar1.Visible = false;
                        btn_genera.Visible = true;
                        btn_genera.Enabled = true;
                        groupBox1.Visible = true;
                        groupBox2.Visible = false;
                        progressBar1.Value = 0;                        
                    }
                }
            }
            else
            {
                MessageBox.Show("Selecciona una carpeta", "Warnign", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btn_rutabd_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Seleccione la base de datos";
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Access Documents|*.accdb;*.mdb";
            openFileDialog1.ShowDialog();
            rutaBD.Text = openFileDialog1.FileName;
        }

        private void btn_conecta_Click(object sender, EventArgs e)
        {
            if (rutaBD.Text == "")
                MessageBox.Show("Selecciona una base de datos","Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                try
                {
                    DB = new DBAccess(rutaBD.Text, contrasena.Text);

                    groupBox1.Visible = false;
                    groupBox2.Visible = true;                        
                }
                catch
                {
                    MessageBox.Show("Error al conectarse con la base de datos. Verifique los datos","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btn_destino_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            destino.Text = folderBrowserDialog1.SelectedPath;
        }

        private void btn_rutaxml_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
            {
                rutaXMLs.Text = folderBrowserDialog2.SelectedPath;
            }
        }

        private void btn_crearef_Click(object sender, EventArgs e)
        {
            if (rutaXMLs.Text != "")
            {
                string[] files = Directory.GetFiles(rutaXMLs.Text,"*.xml");

                if (files.Length > 0)
                {
                    StreamWriter sw = File.CreateText(rutaXMLs.Text + @"\resources.xml");
                    sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    sw.WriteLine("<resources>");

                    for (int i = 0; i < files.Length - 1; i++)
                    {
                        sw.WriteLine("  <resource name=\"" + Path.GetFileNameWithoutExtension(files[i].ToString()) + "\" configFile=\"" + Path.GetFileName(files[i].ToString()) + "\"/>");
                    }
                    sw.WriteLine("</resources>");
                    sw.Close();
                    MessageBox.Show("Archivo resources.xml creado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else { MessageBox.Show("En la carpeta seleccionada no se encontraron archivos .xml", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            }
            else 
            {
                MessageBox.Show("Selecciona la carpeta donde se encuentran los archivos .xml","Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Esta linea la agrego juan carlos mora 
            MessageBox.Show("Saludos cambio en visual studio");
        }
    }
}
